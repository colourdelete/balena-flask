'''
Placeholder docstring
'''

from flask import Flask # pylint: disable=import-error
app = Flask(__name__)

@app.route('/')
def hello_world():
    '''
    Handler for "/".
    '''
    return 'Hello World!'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
